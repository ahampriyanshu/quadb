import React from "react";
import "./Footer.css";
import {Link} from '@material-ui/core';

function Footer() {
  return (
    <div className="footer">
      <hr></hr>
      <div className="footerBody">
        <div className="footerInfo">
          <div>Copyright © 2022</div>
          <div>HodlInfo.com</div>
          
          <div style={{ display: "flex" }}>
            Developed By&nbsp;<span><Link
        color="inherit"
        underline="none"
        href="https://www.quadbtech.com/"
      >QuadBTech</Link></span>
          </div>
        </div>
        <div className="footerContact">
        <div><Link
        color="inherit"
        underline="none"
        href="mailto:support@hodlinfo.com"
      >Support</Link></div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
