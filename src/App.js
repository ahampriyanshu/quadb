import "./App.css";
import { useState } from "react";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";

function App() {
  const [darkTheme, setDarkTheme] = useState(true);
  function handleToggleTheme() {
    setDarkTheme(!darkTheme);
  }
  return (
    <div className={`App ${!darkTheme && "white__theme"}`}>
      <Header darkTheme={darkTheme} handleToggleTheme={handleToggleTheme} />
      <Footer />
    </div>
  );
}

export default App;
